# Databricks Git Hooks
Local git hooks for automatically importing source code files to the Databricks
workspace.

This assumes you have already created the `databricks` conda environment per
[these instructions.](CLI_setup.md)

## Setup
These must be added to the git hooks folder for any repository from which you
are working with Databricks code (i.e. they are not applied globally).

First, clone this repo
```bash
git clone git@bitbucket.org:halpoins/dbc-git-hooks.git
```

Copy these files to the `.git/hooks` folder of your project (**pipelines**, for example). 
_Note: it's possible to hard-link them to the hooks folder so that bug fixes to this repo 
perpetuate to your projects._
```bash
cd dbc-git-hooks
cp prepare-commit-msg ../pipelines/.git/hooks/
cp post-commit ../pipelines/.git/hooks/

# This one goes at the root of your project
cp dbc_import.py ../pipelines/
```

Copy the sample directory mappings to your project space, which is your personal 
roadmap for where you want files to go in the Databricks workspace (be sure to 
name it `hook_dir_mappings.yaml`)
```bash
cp sample_hook_dir_mappings.yaml ../pipelines/hook_dir_mappings.yaml
```

### Your personal modifications
Now edit the `post-commit` bash script where you copied it to your project's `hooks`
folder, so the path to the python script is correct
```bash
# ENSURE THIS PATH POINTS TO THE RIGHT PLACE >>>>>>>>>>>>>>>>>>>>
OUTPUT=$(python $USERPROFILE\\Documents\\Projects\\pipelines\\dbc_import.py $line)
# <<<<<<<<<<<<<<<<<<<<<<<<<<
```

In the copy of `hook_dir_mappings.yaml` you put in your project folder, specify
which files go where in the Databricks workspace
```yaml
WordFilterTriggerV1_1.py: '/Users/DTORKELSON/wordfilter/wordfilter_trigger'
```

## Usage
### Quick usage
```bash
git commit -am "dbc: refactor wordfilter trigger"
```

### Detailed usage
Make changes to `.py` or `.scala` files, then when you `git commit` you should 
see something like this
```bash

# dbc: UNCOMMENT THIS LINE TO SEND THESE:
#	WordFilterTriggerV1_1.py --> /Users/DTORKELSON/wordfilter/wordfilter_trigger

# Uncomment the line above if you want to push the changed
# file(s) to the Databricks workspace. NOTE: changing the destination
# paths here will not have any effect on where they are uploaded. To
# change that, you should quit out of this (with an empty commit msg)
# and modify the hook_dir_mappings.yaml file, then try again.
```

Write your commit message at the top, and remove the `#` from the `dbc:` line
in order to push that file up to Databricks. It should look like this:
```bash
refactor wordfilter trigger

 dbc: UNCOMMENT THIS LINE TO SEND THESE:
#	WordFilterTriggerV1_1.py --> /Users/DTORKELSON/wordfilter/wordfilter_trigger
```

A successful import will look something like this; you want to see `returncode=0` at the bottom.
```
Found the dbc hook call, proceeding with import.
        SOURCE: C:\Users\dtorkelson\Documents\Projects\pipelines\trigger\wordfilter\WordFilterTriggerV1_1.py
        DEST: /Users/DTORKELSON/wordfilter/wordfilter_trigger
CompletedProcess(args=['C:\\Users\\dtorkelson\\AppData\\Local\\Continuum\\anaconda3\\envs\\databricks\\Scripts\\databricks', 'workspace', 'import', '--language', 'python', '--overwrite', 'C:\\Users\\dtorkelson\\Documents\\Projects\\pipelines\\trigger\\wordfilter\\WordFilterTriggerV1_1.py', '/Users/DTORKELSON/wordfilter/wordfilter_trigger'], returncode=0, stdout='', stderr='')
```

### To skip hooks for a specific command
_Very_ useful for large rebases!
```
git -c core.hooksPath=/dev/null some-git-command
```

