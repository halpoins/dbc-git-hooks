"""
Import local source files to Databricks Workspace using Databricks CLI.

"""
import sys
from os import path
import yaml
import subprocess


def main():
    input_from_bash = sys.argv[1]

    proj_root_path = path.normpath(path.join(path.dirname(path.realpath(__file__))))
    source_path = path.join(proj_root_path, path.normpath(input_from_bash))
    source_file = path.basename(source_path)

    config_file = path.join(proj_root_path, 'hook_dir_mappings.yaml')
    config_dict = get_src_tgt_mappings(config_file)
    try:
        dest_path = config_dict[source_file]
    except KeyError:
        pass  # file not given a destination in the mappings
    else:
        result = send_to_databricks(src_path=source_path, dest_path=dest_path)
        print(result)
    return 


def get_src_tgt_mappings(config_file: str) -> dict:
    """Get the mappings of source directories to workspace directories.

    For example, your source code file may be at pipelines/wordfilter/trigger.py,
    but you want it to go in the Databricks workspace as 
        /[COMMON_WORKSPACE]/analytics/wordfilter/python/wordfilter_trigger.py

    And this is assumed to be a fairly permanent decision; however, it will be
    determined on a per-file or per-directory basis, so it should be stored
    in a config file.

    """
    with open(config_file, 'r') as f:
        config = yaml.safe_load(f)
    return config


def send_to_databricks(src_path: str, dest_path: str) -> subprocess.CompletedProcess:
    """Use the Databricks CLI to upload file."""
    lang = determine_script_language(src_path=src_path)
    print(f'\tSOURCE: {src_path}')
    print(f'\tDEST: {dest_path}')

    databricks_cli_path = path.join(path.expanduser('~'), 'AppData', 'Local', 'Continuum',
        'anaconda3', 'envs', 'databricks', 'Scripts', 'databricks')
    result = subprocess.run([
        databricks_cli_path, 'workspace', 'import', '--language', lang, 
        '--overwrite', src_path, dest_path], capture_output=True, text=True)
    return parse_result_output(result)


def determine_script_language(src_path: str) -> str:
    """Determine language from file extension.

    Defaults to python.
    """
    extension_mapping = {
        '.py': 'python',
        '.scala': 'scala',
        '.sql': 'sql'
    }
    filename, file_ext = path.splitext(src_path)
    try:
        lang = extension_mapping[file_ext]
    except KeyError:
        lang = 'python'
    finally:
        return lang


def parse_result_output(result: subprocess.CompletedProcess) -> str:
    """Clean up the output of the ``databricks import`` call."""
    subprocess_output = str(result)
    success_message = '\nDatabricks workspace import successful.\n'
    fail_message = '\n'.join(['', 'Databricks workspace import UNSUCCESSFUL.',
        'Inspect the output for errors.', ''])
    if result.returncode == 0:
        return '\n'.join([subprocess_output, success_message])
    else:
        return '\n'.join([subprocess_output, fail_message])



if __name__ == '__main__':
    main()
