# Databricks CLI Setup

## 1. Install Anaconda (if not already)

## 2. Create a conda environment specifically for this

Create a file `databricks_conda.yaml` from the following
```yaml
name: databricks
dependencies:
  - python=3.7
  - pip
  - pip:
    - databricks-cli
```

then run this in **Anaconda Prompt**:
```
conda env create -f databricks_conda.yaml
```

Then
```
conda activate databricks
```

## 3. Create your personal access token in Databricks

1. Click your username menu, then User Settings
2. Go to Access Tokens 
3. Click Generate New Token
4. Clear out the lifetime field, if you want it to persist forever
5. Copy the resulting token and save it somewhere

## 4. Point `databricks-cli` to the Databricks instance via your token

**Note:** By using `--profile preprod` here, you are naming the profile and will be required 
to reference that name for every command you send. This might be important for those working on
multiple Databricks instances, but I think it's safe to name it "DEFAULT" if it's your only one.
```
databricks configure --insecure --token --profile preprod
```
When prompted, paste this URL (a right-click in Anaconda Prompt will paste what's on the clipboard):
```
<databricks instance URL>
```

Next it asks for your token, but treats it like a password so you won't be able to see what you're
typing. I am not sure if pasting works, but there is a way to check it later (see below on the 
config file).

Test it with
```
databricks fs ls dbfs:/ --profile preprod
```

### Where this configuration is stored
In your home directory (`%USERPROFILE%`, should be C:/Users/you), you now have a file 
`.databrickscfg` with the profile you created. In this file you can change the name of the profile
(for example, changing it from `prprod` to `DEFAULT` to save yourself some CLI effort) and
also edit your token if you mis-entered it. 

In theory, I suppose you could create the file prior to step 4 and skip the command-line entry
portion.

